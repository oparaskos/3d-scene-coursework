#ifndef SCENE_H
#define	SCENE_H

#include <string>
#include "GLInclude.h"
#include "Asset.h"
#include "Object.h"
#include "Light.h"
#include <list>


class Scene : Asset {
public:
	Scene();
	Scene(const std::string& filepath);
	virtual ~Scene();
	virtual bool isValid() const override;
	void setMatrices(const glm::mat4&, const glm::mat4&, const glm::mat4&);

	void Draw(DestinationFramebufferType framebuffer) const;
	// TODO:
	//	glm::mat4 getViewMatrix() const;
	//	glm::mat4 setupViewFrustum() const; // limit view frustm to furthest object
	//	glm::mat4 getProjectionMatrix() const;
	//	//std::vector<Surface> findLightsAndEntities() const; //return depth sorted surfaces
	//	//	void addLightSurfaces(std::list<Surface>& /*inout*/) const; //add surfaces that lights hit (ie baddies round corners) that cast shadows
	//	//	void addModelSurfaces(std::list<Surface>& /*inout*/) const; // add any other surfaces
	//	//	void sortDrawSurfaces(std::list<std::list<Surface> >) const;
	//	void fillZBuffer(glm::mat4 viewFrustum);

	void setCameraLocation(glm::vec3 cam, glm::vec3 lookAt);
	glm::vec3 getCamera();
	glm::vec3 getLookAt();

protected:
	// Shouldnt be able to perform load outside of creation.
	void Load(const std::string &filename) override;

private:
	bool valid;
	std::list<Object> objectList;
	std::vector<Light> lightList;
	glm::mat4 projection;
	glm::vec3 cameraPosition;
	glm::vec3 lookAtPosition;
};

#endif