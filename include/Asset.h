#ifndef IASSET_H
#define	IASSET_H
#include <string>

class Asset {
public:
	Asset(){}
	virtual ~Asset(){}

	virtual bool isValid() const = 0;
protected:
	// Shouldn't be able to perform load outside of creation.
	virtual void Load(const std::string&) = 0;
	virtual void LoadDefault(){}
};

#endif	/* IASSET_H */

