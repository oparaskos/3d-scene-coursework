#ifndef OBJECT_H
#define OBJECT_H

#include "GLInclude.h"
#include "MeshInstance.h"

#include <list>

class Object
{
public:
	Object();
	~Object();

	void setTranslation(glm::vec3, glm::vec3, float);
	void setVelocity(glm::vec3, glm::vec3, float);
	void addMeshInstance(const MeshInstance& meshInstance);

	void Draw(DestinationFramebufferType framebuffer) const;
	void setMatrices(const glm::mat4&, const glm::mat4&, const glm::mat4&);

private:
	std::list<MeshInstance> meshInstances;
};

#endif