#ifndef OBSERVABLE_H
#define OBSERVABLE_H

class Observable {
public:
	Observable(){}
	virtual ~Observable(){}
	virtual void Update(int message, void* data) = 0;
};

#endif