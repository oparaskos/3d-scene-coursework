#ifndef TEXTURE_HANDLE_H
#define TEXTURE_HANDLE_H

#include <unordered_map>
#include <iostream>
#include <string>

#include "GLInclude.h"
#include "Texture.h"
#include "Singleton.h"

#include <memory>

typedef std::shared_ptr<Texture> textureptr;
typedef std::unordered_map<std::string, textureptr> texture_map;
typedef std::pair<std::string, textureptr> texture_pair;
typedef texture_map::iterator texture_iterator;

class TextureHandler : public Singleton<TextureHandler> {
public:
	TextureHandler(){}
	virtual ~TextureHandler() override {}
	textureptr operator[](const std::string &filename);
	size_t size();

private:
	// Copying not supported:
	TextureHandler(const TextureHandler&);
	TextureHandler& operator=(const TextureHandler&);
	
private:
	texture_map mHandleMap;
};

#endif


