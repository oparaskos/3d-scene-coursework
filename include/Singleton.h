#ifndef SINGLETON_H
#define SINGLETON_H

template <class T>
class Singleton {
public: 
	static T& GetSingleton()
	{
		static T sTextureHandler;
		return sTextureHandler;
	}
protected:
	Singleton(){}
	virtual ~Singleton(){}
};

#endif