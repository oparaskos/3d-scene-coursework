#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "ImageBuffer.h"
#include "Texture.h"

typedef handle framebuffer_handle;

class Framebuffer : public Imagebuffer
{
public:
	Framebuffer(int sizex = 1024, int sizey = 1024);
	virtual ~Framebuffer();

	virtual handle getHandle() const override;
	virtual void Bind() const override;
	virtual void BindDefault() const override;
	virtual bool isValid() const override;

	void attachToTexture(const Texture&, int);

private:
	framebuffer_handle handle;
	int width;
	int height;
};

#endif