#include <SDL_thread.h>
#include <string>
#include <unordered_map>
#include <queue>

#include "Delegate.h"

#ifndef TASK_HANDLER_H
#define TASK_HANDLER_H

typedef SDL_ThreadPriority ThreadPriority;
typedef std::pair< ThreadPriority, Delegate > Task;


/*final*/ class TaskHandler {
public:
	TaskHandler();
	~TaskHandler();

	class TaskCollection {
	public:
		TaskCollection();
		TaskCollection(const std::string& threadName);
		~TaskCollection();
		size_t Size();
		int Await();

		friend int __cdecl Scheduler(void* taskCollection);

		void QueueTask(Delegate task, ThreadPriority priority = ThreadPriority::SDL_THREAD_PRIORITY_NORMAL) {
			jobs.push(Task(priority, task));
		}

	private:
		SDL_Thread* thread;
		std::queue<Task> jobs;
	};

	bool CreateWorkerThread();
	bool CreateSpecialisedThread(const std::string& threadId);

	TaskCollection& GetWorkerThread(); // Get the least full Worker Thread
	TaskCollection& GetSpecialisedThread(std::string threadName); // Get the specialised thread by name

private:
	// Non specialised threads meant for non specific asynchronous jobs
	std::list< TaskCollection > workerThreads;
	// Specialised Threads use normal (non-priority) queue for tasks
	std::unordered_map< std::string, TaskCollection > specialisedThreads;
};

int __cdecl Scheduler(void* taskCollection);

#endif