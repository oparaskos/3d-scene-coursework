#ifndef LIGHT_H
#define LIGHT_H
#include "GLInclude.h"
class Light{
public:
	Light(glm::vec3, float);
	operator glm::vec4();

	glm::vec3 getPosition();
	float getIntensity();
private:
	glm::vec3 position;
	float intensity;
};
#endif