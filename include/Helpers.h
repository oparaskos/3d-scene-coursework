#ifndef HELPERS_H
#define HELPERS_H

#include "GLInclude.h"
#include <algorithm>
#include "json\json.h"
#include <string>

/// Convert a Json::Value to a glm::vec3
/// <param name="v"> json value to convert </param>
/// <param name="xstr"> name of the x parameter </param>
/// <param name="ystr"> name of the y parameter </param>
/// <param name="zstr"> name of the z parameter </param>
/// return pair(bool, vec3) first is true when xstr, ystr and zstr are all found vec3 is (-1, -1, -1) when they are not.
inline std::pair<bool, glm::vec3> toVec3(Json::Value v, const std::string& xstr, const std::string& ystr, const std::string& zstr);

/// Convert a Json::Value to a glm::vec3 using default xy and zstr
/// <param name="v"> json value to convert </param>
/// return vec3 is (-1, -1, -1) when they are not.
glm::vec3 toVec3(Json::Value v);

#ifndef M_PI
#define M_PI 3.14157
#endif
#define F_PI (float) M_PI

inline float radians(float degrees) {
	return degrees * F_PI / 180.0f;
}

#endif // HELPERS_H