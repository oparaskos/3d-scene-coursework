#ifndef SAMPLER_H
#define SAMPLER_H

#include "GLInclude.h"

typedef GLuint sampler_handle;

class Sampler {
public:
	Sampler(GLenum magFilter, GLenum minFilter, GLenum wrap);
	virtual ~Sampler();
	void Init(GLenum magFilter, GLenum minFilter, GLenum wrap);	
	sampler_handle getHandle() const;

private:
	sampler_handle handle;
};

#endif