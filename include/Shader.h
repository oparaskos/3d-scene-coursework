#ifndef SHADER_H
#define	SHADER_H

#include <string>
#include "GLInclude.h"
#include "Asset.h"

typedef GLuint shader_handle;
enum { shader_handle_invalid = 0 };

class Shader : Asset {
public:
	Shader(){}
	Shader(const std::string& filepath, GLenum type);
	Shader(shader_handle, GLenum);
	virtual ~Shader();
	
	shader_handle getHandle() const;
	void Bind() const;
	virtual bool isValid() const override;
	
protected:
	// Shouldnt be able to perform load outside of creation.
	void Load(const std::string &filename) override;
	shader_handle Build(const GLchar** source, GLenum type);
private:
	shader_handle handle;
	GLenum type;
};

#endif	/* TEXTURE_H */

