#ifndef SHADERHANDLER_H
#define SHADERHANDLER_H

#include <unordered_map>
#include <iostream>
#include <string>

#include "GLInclude.h"
#include "Program.h"
#include "Singleton.h"

#include <memory>

typedef std::shared_ptr<Program> programptr;
typedef std::unordered_map<std::string, programptr> program_map;
typedef std::pair<std::string, programptr> program_pair;
typedef program_map::iterator program_iterator;

class ShaderHandler : public Singleton<ShaderHandler> {
public:
	ShaderHandler(){}
	virtual ~ShaderHandler() override {}
	programptr operator[](const std::string &filename);
	size_t size();

private:
	// Copying not supported:
	ShaderHandler(const ShaderHandler&);
	ShaderHandler& operator=(const ShaderHandler&);

private:
	program_map mHandleMap;
};

#endif


