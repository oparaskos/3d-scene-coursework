#ifndef STATICMESHCOMPONENT_H
#define STATICMESHCOMPONENT_H

#include "Component.h"
#include "MeshHandler.h"

class StaticMeshComponent : public Component {
public:
	StaticMeshComponent(const std::string& filepath);

private:
	const Mesh& mesh;
};

#endif