#ifndef PROGRAM_H
#define	PROGRAM_H

#include <string>
#include "GLInclude.h"
#include "Asset.h"
#include <vector>
#include <memory>
#include "Texture.h"
#include "Sampler.h"
#include "Light.h"
#include <string>

typedef GLuint program_handle;
typedef GLint uniform_location;
enum { program_handle_invalid = 0 };
enum { MAX_LIGHTS = 12 };
class Program : Asset {
public:
	class UniformLocation {
		typedef GLint uniform_handle;
	public:
		UniformLocation(const std::string&, Program&);
		operator GLuint();

		UniformLocation& operator= (const GLfloat&);
		UniformLocation& operator= (const GLuint&);
		UniformLocation& operator= (const glm::mat4& lhs);
		UniformLocation& operator= (const glm::vec4& lhs);
		UniformLocation& operator= (const std::vector<Light>& lhs);

		bool isValid();
		operator bool();

		void BindTexture(int, const Texture&);
		void BindTexture(int, const Sampler&, const Texture&);
		uniform_handle getHandle();
	private:
		uniform_handle location;
		const std::string locationName;
		Program* program;
	};

public:
	Program(const std::string&);
	virtual ~Program();
	
	program_handle getHandle() const; 
	operator GLuint();
	inline void Bind() const;
	virtual bool isValid() const override;
	Program::UniformLocation operator[](const std::string&);
	Program::UniformLocation Uniform(const std::string&);
	bool hasTransparency();
	void setTransparency(bool);
	const std::string getName();
	
protected:
	// Shouldn't be able to perform load outside of creation.
	void Load(const std::string&) override;
private:
	program_handle handle;
	GLenum type;
	bool transparency;
	const std::string programName;
};

#endif	/* TEXTURE_H */

