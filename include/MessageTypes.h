#ifndef MESSAGETYPES_H
#define MESSAGETYPES_H

enum MessageTypes
{
	/* General Messages: */
	PREUPDATE,
	UPDATE,
	POSTUPDATE,

	PRERENDER,
	RENDER,
	POSTRENDER,

	SUBSCRIBE,
	UNSUBSCRIBE,

	/* ShaderProgram Specific Messages: */
	UNIFORMUPDATE,

	END
};

#endif
