#ifndef GLINCLUDE_H
#define	GLINCLUDE_H

// Include GL & GLEW
#ifdef __APPLE__
#	include <OpenGL/glew.h>
#	include <OpenGL/gl.h>
#	include <OpenGL/glu.h>
#else
#	ifdef _WIN32
#		define WIN32_LEAN_AND_MEAN
#		include <Windows.h>
#	endif
#	include <GL/glew.h>
#	include <GL/gl.h>
#	include <GL/glu.h>
#endif
// Include SDL2
#include <SDL.h>
#include <SDL_image.h>
// Include GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum DestinationFramebufferType
{
	OUTPUT = 0,
	TRANSPARENTOBJECTS = 1,
	SOLIDOBJECTS = 2
};

#endif	/* GLINCLUDE_H */

