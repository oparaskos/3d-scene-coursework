#include <algorithm>
#include <cassert>

typedef void* any_ptr;

/// <summary>
/// A class that wraps up function pointers nicely so that methods and functions can be passed in the same way as arguments.
/// From http://molecularmusings.wordpress.com/2011/09/19/generic-type-safe-delegates-and-events-in-c/
/// </summary>
class Delegate
{
	typedef void* InstancePtr;
	typedef void(*InternalFunction)(InstancePtr, std::vector<any_ptr>);
	typedef std::pair<InstancePtr, InternalFunction> Stub;

	// turns a free function into our internal function stub
	template <void(*Function)(std::vector<any_ptr> arguments)>
	static void FunctionStub(InstancePtr, std::vector<any_ptr> arguments)
	{
		// we don't need the instance pointer because we're dealing with free functions
		return (Function)(arguments);
	}

	// turns a member function into our internal function stub
	template <class C, void (C::*Function)(std::vector<any_ptr>)>
	static void ClassMethodStub(InstancePtr instance, std::vector<any_ptr> arguments)
	{
		///<TODO>Mutex Locks to make thread safe...</TODO>
		// cast the instance pointer back into the original class instance
		return (static_cast<C*>(instance)->*Function)(arguments);
	}

public:
	Delegate(void)
		: m_stub(nullptr, nullptr)
	{
	}

	/// Binds a free function
	template <void(*Function)(std::vector<any_ptr>)>
	void Bind(void)
	{
		m_stub.first = nullptr;
		m_stub.second = &FunctionStub<Function>;
	}

	/// Binds a class method
	template <class C, void (C::*Function)(std::vector<any_ptr>)>
	void Bind(C* instance)
	{
		m_stub.first = instance;
		m_stub.second = &ClassMethodStub<C, Function>;
	}

	/// Invokes the delegate
	void Invoke(std::vector<any_ptr> arguments) const
	{
		assert(m_stub.second != nullptr && "Cannot invoke unbound delegate. Call Bind() first.");
		return m_stub.second(m_stub.first, arguments);
	}

private:
	Stub m_stub;
};