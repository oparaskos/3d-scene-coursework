#ifndef TEXTURE_H
#define	TEXTURE_H

#include <string>
#include "GLInclude.h"
#include "ImageBuffer.h"

typedef Uint8 byte;
typedef GLuint texture_handle;
enum {texture_handle_invalid = 0};

class Texture : Imagebuffer {
public:
	Texture(const std::string& filepath);
	Texture(texture_handle, int, int, GLenum);
	Texture(byte* pixelData, int, int, GLenum = GL_RGB, GLenum = GL_RGB, GLenum = GL_FLOAT);
	virtual ~Texture();
	
	virtual texture_handle getHandle() const;
	virtual void Bind() const;
	virtual void Bind(int textureUnit) const;
	virtual void BindDefault() const override;
	virtual bool isValid() const override;
	
protected:
	// Shouldnt be able to perform load outside of creation.
	virtual void Load(const std::string &filename) override;
	virtual void LoadDefault() override;
	
private:
	texture_handle handle;
	int width;
	int height;
	GLenum textureFormat;
};

#endif	/* TEXTURE_H */

