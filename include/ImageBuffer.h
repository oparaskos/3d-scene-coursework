#ifndef IMAGEBUFFER_H
#define	IMAGEBUFFER_H

#include <string>
#include "GLInclude.h"
#include "Asset.h"
#include <iostream>

typedef GLuint handle;

class Imagebuffer : Asset {
public:
	Imagebuffer(){}
	virtual ~Imagebuffer(){}

	virtual handle getHandle() const = 0;
	virtual void Bind() const = 0;
	virtual void BindDefault() const = 0;
	virtual bool isValid() const override =0;

	GLenum getAttachment(int attachmentPoint)
	{
		GLenum attachment = GL_COLOR_ATTACHMENT0;
		switch (attachmentPoint)
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
			attachment += attachmentPoint;
			break;
		case GL_DEPTH_ATTACHMENT:
		case GL_STENCIL_ATTACHMENT:
		case GL_COLOR_ATTACHMENT0:
		case GL_COLOR_ATTACHMENT1:
		case GL_COLOR_ATTACHMENT2:
		case GL_COLOR_ATTACHMENT3:
		case GL_COLOR_ATTACHMENT4:
		case GL_COLOR_ATTACHMENT5:
		case GL_COLOR_ATTACHMENT6:
		case GL_COLOR_ATTACHMENT7:
		case GL_COLOR_ATTACHMENT8:
		case GL_COLOR_ATTACHMENT9:
		case GL_COLOR_ATTACHMENT10:
		case GL_COLOR_ATTACHMENT11:
		case GL_COLOR_ATTACHMENT12:
		case GL_COLOR_ATTACHMENT13:
		case GL_COLOR_ATTACHMENT14:
		case GL_COLOR_ATTACHMENT15:
			attachment = attachmentPoint;
			break;
		default:
			std::cerr << "Invalid Attachment Point " + attachmentPoint
				<< "Valid attachment points is [0 <= x <= 15], GL_DEPTH_ATTACHMENT, and GL_STENCIL_ATTACHMENT" << std::endl;
			attachment = attachmentPoint;
			break;
		}
		return attachment;
	}

protected:
	// Shouldnt be able to perform load outside of creation.
	virtual void Load(const std::string &filename) override{};
	virtual void LoadDefault() override{};
};

#endif	/* TEXTURE_H */

