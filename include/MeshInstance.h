#ifndef MESHINSTANCE_H
#define MESHINSTANCE_H
#include <string>
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"
#include <vector>

class MeshInstance
{
public:
	MeshInstance();
	~MeshInstance();

	void setURI(const std::string& uri);
	void setProgram(const std::string& uri);
	void addTexture(const std::string& uri, const std::string& location);
	void setTranslation(glm::vec3, glm::vec3, float);
	void setVelocity(glm::vec3, glm::vec3, float);

	void Draw(DestinationFramebufferType framebuffer) const;

	void setMatrices(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj);
	void setLightList(const std::vector<Light>*);

private:
	meshptr mesh;
	programptr program;
	typedef std::pair<textureptr, std::string> texture_location;
	std::list<texture_location> textures;
	const std::vector<Light>* pLightList;
	glm::vec3 position, velocity;
	glm::vec4 rotation, rvelocity;
};

#endif // MESHINSTANCE_H
