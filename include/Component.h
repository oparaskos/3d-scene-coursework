#ifndef ICOMPONENT_H
#define ICOMPONENT_H

#include "IObserver.h"
#include "MessageTypes.h"

class Component : public IObserver {
public:
	Component(){}
	virtual ~Component(){}
	virtual void Update(int message, void* data)
	{
		switch (message)
		{
		case MessageTypes::UPDATE:
			Update(data);
			break;
		case MessageTypes::PREUPDATE:
			PreUpdate(data);
			break;
		case MessageTypes::POSTUPDATE:
			PostUpdate(data);
			break;
		case MessageTypes::PRERENDER:
			PreRender(data);
			break;
		case MessageTypes::RENDER:
			Render(data);
			break;
		case MessageTypes::POSTRENDER:
			PostRender(data);
			break;
		case MessageTypes::SUBSCRIBE:
			Subscribe(data);
			break;
		case MessageTypes::UNSUBSCRIBE:
			Unsubscribe(data);
			break;
		default:
			break;
		}
	}

	virtual void PreUpdate(void* data){}
	virtual void Update(void* data){}
	virtual void PostUpdate(void* data){}

	virtual void PreRender(void* data){}
	virtual void Render(void* data){}
	virtual void PostRender(void* data){}

	virtual void Subscribe(void* data){}
	virtual void Unsubscribe(void* data){}
};

#endif