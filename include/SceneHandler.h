#ifndef SCENE_HANDLER_H
#define SCENE_HANDLER_H

#include <unordered_map>
#include <iostream>
#include <string>

#include "GLInclude.h"
#include "Scene.h"
#include "Singleton.h"

#include <memory>

typedef std::shared_ptr<Scene> sceneptr;
typedef std::unordered_map<std::string, sceneptr> scene_map;
typedef std::pair<std::string, sceneptr> scene_pair;
typedef scene_map::iterator scene_iterator;

class SceneHandler : public Singleton<SceneHandler> {
public:
	SceneHandler(){}
	virtual ~SceneHandler() override {}
	sceneptr operator[](const std::string &filename);
	size_t size();

private:
	// Copying not supported:
	SceneHandler(const SceneHandler&);
	SceneHandler& operator=(const SceneHandler&);

private:
	scene_map mHandleMap;
};

#endif


