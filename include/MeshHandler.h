#ifndef MESH_HANDLE_H
#define MESH_HANDLE_H

#include <unordered_map>
#include <iostream>
#include <string>

#include "GLInclude.h"
#include "Mesh.h"
#include "Singleton.h"

#include <memory>

typedef std::shared_ptr<Mesh> meshptr;
typedef std::unordered_map<std::string, meshptr> mesh_map;
typedef std::pair<std::string, meshptr> mesh_pair;
typedef mesh_map::iterator mesh_iterator;

class MeshHandler : public Singleton<MeshHandler> {
public:
	MeshHandler(){}
	virtual ~MeshHandler() override {}
	meshptr operator[](const std::string &filename);
	size_t size();

private:
	// Copying not supported:
	MeshHandler(const MeshHandler&);
	MeshHandler& operator=(const MeshHandler&);
	
private:
	mesh_map mHandleMap;
};

#endif


