/* 
 * File:   GLError.h
 * Author: oliver
 *
 * Created on 26 March 2014, 21:41
 */

#ifndef GLERROR_H
#define	GLERROR_H

#include "GLInclude.h"
#include <string>
/** @return the type of shader as a string given a GLenum as input*/
std::string ShaderTypeName(GLenum type);
// General function used for checking how a shader compilation went.
// Checks to see if a compile error has been registered and prints it out and exits if so.
void checkGLShaderStatus(GLuint id, GLuint thingToCheck, bool shader);
// General function for compiling a shader.
GLuint buildShader(const GLchar **source, GLenum type);
// Checks for OpenGL state errors and prints out the error and quits if so.
bool checkGL(std::string stage, bool exit = true);
void printVersionInformation(bool printExtensionList = false);
// Clear all errors in gl error stack, returns false if there was an error.
bool clearGlErrors();
void checkFramebufferStatus(GLenum target);
const std::string getErrorString(GLenum error);

#ifndef d_checkGL
#ifdef _DEBUG
#define d_checkGL(x) do{checkGL(x);}while(0)
#else
#define d_checkGL(x) do{}while(0)
#endif // DEBUG
#endif


#endif	/* GLERROR_H */

