#ifndef MESH_H
#define	MESH_H

#include "GLInclude.h"
#include "Asset.h"

typedef GLuint vao_handle;
typedef GLuint vbo_handle;
typedef GLuint ebo_handle;

enum {	vao_handle_invalid = 0,
		vbo_handle_invalid = 0,
		ebo_handle_invalid = 0 };

enum VertexAttribute {
	POSITION = 0,
	COLOUR = 1,
	TEXCOORD = 2,
	NORMAL = 3
};

class Mesh : Asset{
public:
	Mesh(const std::string& filename);
	Mesh(vao_handle vao, vbo_handle vbo, ebo_handle ebo, size_t numVertices);
	virtual ~Mesh();
	
	vao_handle getVertexArrayHandle() const;
	vbo_handle getVertexBufferHandle() const;
	ebo_handle getElementArrayHandle() const;
	size_t getNumberIndices() const;

	virtual bool isValid() const override;
	void Draw(DestinationFramebufferType framebuffer) const;
protected:
	void Load(const std::string& filename) override;
	
private:
	vao_handle VAO;
	vbo_handle VBO;
	ebo_handle EBO;
	size_t numIndices;
};

#endif	/* MESH_H */

