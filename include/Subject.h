#ifndef ISUBJECT_H
#define ISUBJECT_H

#include "Observable.h"
#include <map>
#include <vector>

class Subject {
public:
	Subject(){}
	virtual ~Subject(){}

	virtual void Subscribe(int message, Observable *observer) = 0;
	virtual void Unsubscribe(int message, Observable *observer) = 0;
	virtual void Notify(int message, void* data) = 0;

protected:
	typedef std::vector<Observable*> ObserverList;
	typedef std::map<int, ObserverList> ObserverMap;

	ObserverMap observers;
};

#endif