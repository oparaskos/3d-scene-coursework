#version 330 core

out vec4 fColor;

uniform sampler2D diffuse;
uniform int LightsCount;

smooth in vec3 position;
smooth in vec2 texcoord;
smooth in vec3 normal;

smooth in vec4 lights[12];
smooth in vec3 eyeVec;

void main() {
	vec3 ambientLight = vec3(1.0, 1.0, 1.0);
	vec3 LightColor = ambientLight;
	fColor = texture2D(diffuse, texcoord) * vec4(LightColor, 1);
}