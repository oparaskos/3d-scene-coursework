#version 330 core

out vec3 fColor;

uniform sampler2D diffuse;

smooth in vec4 position;
smooth in vec2 texcoord;

void main() {
	fColor = texture2D(diffuse, texcoord).rgb;
}