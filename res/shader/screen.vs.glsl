#version 330 core

layout (location=0) in vec3 vPosition;
layout (location=2) in vec3 vTexcoord;

smooth out vec4 position;
smooth out vec2 texcoord;

void main() {
	 position = vec4(vPosition, 1.0);
	 texcoord = vTexcoord.st;
	 gl_Position = vec4((texcoord - vec2(0.5,0.5))*2, 0.0, 1.0) ;
}