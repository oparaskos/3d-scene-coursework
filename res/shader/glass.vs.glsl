#version 330 core

layout (location=0) in vec3 vPosition;
layout (location=2) in vec3 vTexcoord;
layout (location=3) in vec3 vNormal;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

smooth out vec4 position;
smooth out vec2 texcoord;
smooth out vec3 normal;

void main() {
	/* Vertex Attributes */
	position = vec4(vPosition, 1.0);
	texcoord = vTexcoord.st;
	normal = vNormal;
	/* Output */
	 gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(vPosition, 1.0);
}