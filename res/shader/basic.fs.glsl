#version 330 core

out vec4 fColor;

uniform sampler2D diffuse;
uniform sampler2D texNormal;

uniform int LightsCount;

smooth in vec3 position;
smooth in vec2 texcoord;
smooth in vec3 normal;

smooth in vec4 lights[12];
smooth in vec3 eyeVec;

void main() {
	vec3 ambientLight = vec3(0.0, 0.0, 0.0);
	vec3 LightColor = ambientLight;
	vec3 vNormal = normalize(normal + texture2D(texNormal, texcoord).rgb);
	
	vec3 diff = vec3(1, 1, 1);
	for(int i = 0; i < LightsCount; ++i) {
		float lightBrightness = lights[i].w;
		vec3 lightVector = lights[i].xyz;
		vec3 lightColour = diff * dot(lightVector, vNormal);
		float specular = pow(max(dot(reflect(-lightVector, vNormal), eyeVec), 0), lightBrightness);
		lightBrightness = max(0.0f, lightBrightness - length(lightVector));
		LightColor += (lightColour + specular)*(lightBrightness/lights[i].w);
	}	

	fColor = texture2D(diffuse, texcoord) * vec4(LightColor, 1);
}