#version 330 core

out vec4 fColor;

uniform sampler2D solidObject;
uniform sampler2D nonSolidObject;

uniform sampler2D solidDepth;
uniform sampler2D nonSolidDepth;

smooth in vec4 position;
smooth in vec2 texcoord;

void main() {
	float nsdepth = texture2D(nonSolidDepth, texcoord).r;
	float sdepth = texture2D(solidDepth, texcoord).r;
	fColor = texture2D(solidObject, texcoord);
	if(nsdepth < sdepth) {
		vec4 nscolor = texture2D(nonSolidObject, texcoord);
		vec2 offset = nscolor.rg;
		offset -= 0.5;
		offset *= 0.05;
		vec2 samplePosition = texcoord + offset;
		samplePosition = vec2(clamp(samplePosition.s, 0, 1), clamp(samplePosition.t, 0, 1));
		vec4 scolor =texture2D(solidObject, samplePosition); 
		fColor = mix(scolor, scolor * nscolor.b, nscolor.b);
	}
}