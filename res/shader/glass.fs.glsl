#version 330 core

out vec3 fColor;

uniform sampler2D diffuse;

smooth in vec4 position;
smooth in vec2 texcoord;
smooth in vec3 normal;

void main() {
	vec4 color = texture(diffuse, texcoord);
	vec2 normal = normalize(normal.xy*2 + color.rg);
	fColor = vec3(normal.s, normal.t, color.b);
}