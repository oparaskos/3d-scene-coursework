#version 330 core

layout (location=0) in vec3 vPosition;
layout (location=2) in vec3 vTexcoord;
layout (location=3) in vec3 vNormal;

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform vec4 Lights[12];
uniform int LightsCount;

smooth out vec3 position;
smooth out vec2 texcoord;
smooth out vec3 normal;

smooth out vec3 eyeVec;

smooth out vec4 lights[12];

void main() {
	vec4 vPos = vec4(vPosition, 1);
	mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
	
	// Send model transformed into world-space	
	position = vec3(ModelMatrix * vPos);
	// normals need to be rotated (scale is not supported)
	normal = vec3(ModelMatrix * vec4(vNormal, 0));
	// lights are already in world-space
	lights = Lights;
	for(int i = 0; i < LightsCount; ++i) {
		// Figure out directions to lights
		vec3 lightVector = normalize(lights[i].xyz - position);
		lights[i] = vec4(lightVector, lights[i].w);
	}

	texcoord = vTexcoord.st;
	gl_Position = MVP * vPos;
	eyeVec = normalize(-vec3(ViewMatrix * ModelMatrix * vPos));

}