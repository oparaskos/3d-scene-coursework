#include "Mesh.h"
#include "GLError.h" //Error Handling
// Include Assimp
#include <assimp/Importer.hpp>	// importer
#include <assimp/scene.h>		// aiScene
#include <assimp/postprocess.h> // aiProcess_*

#include <assimp/mesh.h>
#include <assimp/vector3.h>

#include <iostream>

static const int assimp_process_flags = 
	aiProcess_Triangulate | // Make anything that isn't a triangle into triangles
	aiProcess_JoinIdenticalVertices | // Turn soup into a mesh
	aiProcess_GenNormals | // If for some reason the model file doesn't have normals, calculate them
	aiProcess_GenUVCoords | // Some cleverclogs 3D apps don't bother generating UVs if they do their own maps
	aiProcess_TransformUVCoords | // And some of them also stick part of the UV data in with the texture
	aiProcess_OptimizeGraph | aiProcess_OptimizeMeshes // No rush to load, so optimize all we can
;

Mesh::Mesh(const std::string& filename) {
	VAO = vao_handle_invalid;
	VBO = vbo_handle_invalid;
	EBO = ebo_handle_invalid;
	Load(filename);
}

Mesh::Mesh(vao_handle vao, vbo_handle vbo, ebo_handle ebo, size_t size) {
	VAO = vao;
	VBO = vbo;
	EBO = ebo; 
	numIndices = size;
}

Mesh::~Mesh() {
	glDeleteBuffers(1, &VBO); 
	glDeleteVertexArrays(1, &VAO); 
}

void Mesh::Load(const std::string& filename) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(filename.c_str(), assimp_process_flags);
	if(scene == nullptr) {
		std::cerr	<< "Error Reading Scene '" << filename << "'" << std::endl
					<< importer.GetErrorString() << std::endl;
		return;
	}

	if (!scene->HasMeshes()) {
		std::cerr	<< "Scene Had no Meshes" << std::endl;
		return;
	}

	if (scene->HasTextures()) {
		std::cerr	<< "Warning: Scene has embedded textures, "
					<< "These will need to be unpacked to be loaded separately"
					<< std::endl;
	}
	for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
		const aiMesh* mesh = scene->mMeshes[i];
		/* Get Vertex Data */
		if (mesh->mNumVertices <= 3) {
			std::cerr	<< "Asset importer: not enough triangles!"
						<< std::endl;
			return;
		}
		/* Generate Indices */
		numIndices = mesh->mNumFaces * 3;
		GLushort* indices = new GLushort[numIndices];
		size_t indexLoc = 0;
		for (unsigned int f = 0; f < mesh->mNumFaces; f++) {
			const aiFace* face = &mesh->mFaces[f];
			if (face->mNumIndices > 3) {
				std::cerr	<< "Asset importer: mesh not triangulated!"
							<< std::endl;
				return;
			}
			for (unsigned short s = 0; s < 3; s++) {
				indices[indexLoc++] = face->mIndices[s];
			}
		}
		// Reserve space on the card:
		glGenVertexArrays(1, &VAO);
		d_checkGL("Creating VAO");

		glGenBuffers(1, &VBO);
		d_checkGL("Creating VBO");

		glGenBuffers(1, &EBO);
		d_checkGL("Creating EBO");

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		size_t verticesSize = mesh->mNumVertices * sizeof(mesh->mVertices[0]);
		size_t texcoordSize = mesh->mNumVertices * sizeof(mesh->mTextureCoords[0][0]);
		size_t normalSize   = mesh->mNumVertices * sizeof(mesh->mNormals[0]);

		// Load data into the buffer
		glBufferData(
			GL_ARRAY_BUFFER, 
			verticesSize + texcoordSize + normalSize,
			nullptr,
			GL_STATIC_DRAW
		);
		
		glBufferSubData(
			GL_ARRAY_BUFFER,
			0,
			verticesSize,
			mesh->mVertices
		);

		glBufferSubData(
			GL_ARRAY_BUFFER,
			verticesSize,
			texcoordSize,
			mesh->mTextureCoords[0]
		);

		glBufferSubData(
			GL_ARRAY_BUFFER,
			verticesSize+texcoordSize,
			normalSize,
			mesh->mNormals
		);

		// Specify how the data in the data block should be passed to the shader 
		glVertexAttribPointer(
			VertexAttribute::POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(mesh->mVertices[0]),
			((void *)(0))
		);

		glVertexAttribPointer(
			VertexAttribute::TEXCOORD,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(mesh->mTextureCoords[0][0]),
			((void *)(verticesSize))
		);


		glVertexAttribPointer(
			VertexAttribute::NORMAL,
			3,
			GL_FLOAT,
			GL_FALSE,
			sizeof(mesh->mNormals[0]),
			((void *)(verticesSize+texcoordSize))
		);

		// Enable the vertex attributes we just set up
		glEnableVertexAttribArray(VertexAttribute::POSITION);
		glEnableVertexAttribArray(VertexAttribute::TEXCOORD);
		glEnableVertexAttribArray(VertexAttribute::NORMAL);

		d_checkGL("creating VBO");

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

		glBufferData(
			GL_ELEMENT_ARRAY_BUFFER,
			numIndices * sizeof(GLushort),
			indices,
			GL_STATIC_DRAW
		);
		d_checkGL("Setting up indices");
		delete[] indices;
	}
	return;
}

vao_handle Mesh::getVertexArrayHandle() const
{
	return VAO;
}
vbo_handle Mesh::getVertexBufferHandle() const
{
	return VBO;
}
ebo_handle Mesh::getElementArrayHandle() const
{
	return EBO;
}

size_t Mesh::getNumberIndices() const {
	return numIndices;
}

bool Mesh::isValid() const {
	return	(getVertexArrayHandle() != vao_handle_invalid) &&
			(getVertexBufferHandle() != vbo_handle_invalid) &&
			(getElementArrayHandle() != ebo_handle_invalid);
}

void Mesh::Draw(DestinationFramebufferType framebuffer) const {
	glBindVertexArray(getVertexArrayHandle());
	glBindBuffer(GL_ARRAY_BUFFER, getVertexBufferHandle());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getElementArrayHandle());
	glDrawElements(GL_TRIANGLES, getNumberIndices(), GL_UNSIGNED_SHORT, (void *)0);
}
