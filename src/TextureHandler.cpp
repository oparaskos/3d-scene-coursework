#include "TextureHandler.h"

textureptr TextureHandler::operator[](const std::string &filename) {
	texture_iterator result;
	result = mHandleMap.find(filename);
	if(result != mHandleMap.end()) {
		return result->second;
	} else {
		textureptr loadResult(new Texture(filename));
		if (loadResult->isValid()) {
			mHandleMap.insert(texture_pair(filename, loadResult));
		} else {
			std::cerr << "Failed to load image (\"" << filename << "\")\n";
		}
		return loadResult;
	}
}

size_t TextureHandler::size() { return mHandleMap.size(); }
