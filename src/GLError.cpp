/* 
 * File:   GLError.cpp
 * Author: oliver
 * 
 * Created on 26 March 2014, 21:41
 */

#include "GLError.h"
#include <iostream>
#include <string>
#include <sstream>

/** @return the type of shader as a string given a GLenum as input*/
std::string ShaderTypeName(GLenum type) {
	switch(type) {
#ifdef GL_VERTEX_SHADER
		case GL_VERTEX_SHADER:
			return "Vertex Shader";
#endif
#ifdef GL_FRAGMENT_SHADER
		case GL_FRAGMENT_SHADER:
			return "Fragment Shader";
#endif
#ifdef GL_GEOMETRY_SHADER
		case GL_GEOMETRY_SHADER:
			return "Geometry Shader";
#endif
#ifdef GL_TESS_CONTROL_SHADER
		case GL_TESS_CONTROL_SHADER:
			return "Tesselation Control Shader";
#endif
#ifdef GL_COMPUTE_SHADER
		case GL_COMPUTE_SHADER:
			return "Compute Shader";
#endif
		default:
			return "Unknown Shader";
	}
}

void checkGLShaderStatus(GLuint id, GLuint thingToCheck, bool shader) {
	GLint ret;
	if (shader) glGetShaderiv(id, thingToCheck, &ret); else glGetProgramiv(id, thingToCheck, &ret);
	// If there was an error
	if (ret == GL_FALSE) {
		// Print it out, then halt
		GLint maxLength = 0;
		if (shader) glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength); else glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
		GLchar *shaderErrorLog = (GLchar*)malloc(sizeof(GLchar)*(maxLength + 1));
		if (shader) glGetShaderInfoLog(id, maxLength, &maxLength, shaderErrorLog); else glGetProgramInfoLog(id, maxLength, &maxLength, shaderErrorLog);
		shaderErrorLog[maxLength] = '\0';
		std::cout << shaderErrorLog;
		free(shaderErrorLog);
		int junk;
		std::cin >> junk;
		exit(EXIT_FAILURE);
	}
}

// Clear all errors in gl error stack, returns false if there was an error.
bool clearGlErrors() {
	GLenum error;
	bool hasError = false;
	do {
		error = glGetError();
		hasError |= (error != GL_NO_ERROR);
	} while (error != GL_NO_ERROR);
	return !hasError;
}

// Checks for OpenGL state errors and prints out the error and quits if so.
bool checkGL(std::string stage, bool exit) {
	GLenum error;
	bool hasError = false;
	do {
		error = glGetError();
		if(error != GL_NO_ERROR){
			hasError = true;
			std::cerr << getErrorString(error) << std::endl;
		}
	} while ( error != GL_NO_ERROR );
	
	if(hasError) {
		if(!exit) std::cout << "Ignoring..." << std::endl;
		std::cout << " (" << stage << ")" << std::endl;
		if(exit) {
			int junk;
			std::cin >> junk;
			::exit(EXIT_FAILURE);
		}
	}
	return !hasError;
}

#define USE_DETAILED_ERRORS 1
const std::string getErrorString(GLenum error) {
	std::stringstream str;
	switch (error)
	{
	case GL_NO_ERROR:
		str << "GL_NO_ERROR" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "No error has been recorded.The value of this symbolic constant is guaranteed to be 0.";
#endif
		break;
	case GL_INVALID_ENUM:
		str << "GL_INVALID_ENUM" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "An unacceptable value is specified for an enumerated argument.The offending command is ignored and has no other side effect than to set the error flag.";
#endif
		break;
	case GL_INVALID_VALUE:
		str << "GL_INVALID_VALUE" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "A numeric argument is out of range.The offending command is ignored and has no other side effect than to set the error flag.";
#endif
		break;
	case GL_INVALID_OPERATION:
		str << "GL_INVALID_OPERATION" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "The specified operation is not allowed in the current state.The offending command is ignored and has no other side effect than to set the error flag.";
#endif
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		str << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "The command is trying to render to or read from the framebuffer while the currently bound framebuffer is not framebuffer complete(i.e.the return value from glCheckFramebufferStatus is not GL_FRAMEBUFFER_COMPLETE).The offending command is ignored and has no other side effect than to set the error flag.";
#endif
		break;
	case GL_OUT_OF_MEMORY:
		str << "GL_OUT_OF_MEMORY" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "There is not enough memory left to execute the command.The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
#endif
		break;
	default:
		str << "UNKNOWN_ERROR" << std::endl;
#ifdef USE_DETAILED_ERRORS
		str << "Could not Convert error code into string." << error;
#endif
		break;
	}
	return str.str();
}

void checkFramebufferStatus(GLenum target)
{
	switch (target)
	{
	case GL_DRAW_FRAMEBUFFER:
	case GL_READ_FRAMEBUFFER:
	case GL_FRAMEBUFFER:
		break;
	default:
		std::cerr << "checkFramebufferStatus: target must be GL_DRAW_FRAMEBUFFER, GL_READ_FRAMEBUFFER or GL_FRAMEBUFFER.GL_FRAMEBUFFER is equivalent to GL_DRAW_FRAMEBUFFER.\n";
		break;
	}
	GLenum error = glCheckFramebufferStatus(target);
	switch (error) {
	case GL_FRAMEBUFFER_UNDEFINED:
std::cerr << R"(GL_FRAMEBUFFER_UNDEFINED, 
	GL_FRAMEBUFFER_UNDEFINED is returned if target is the default framebuffer, but the default framebuffer does not exist.)" << std::endl;
break;
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT 
	GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT is returned if any of the framebuffer attachment points are framebuffer incomplete.)") << std::endl;

break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT 
	GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT is returned if the framebuffer does not have at least one image attached to it.)") << std::endl;
break;
	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER  
	GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER is returned if the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAW_BUFFERi)") << std::endl;
break;
	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER   
	GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER is returned if GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER.)") << std::endl;
break;
	case GL_FRAMEBUFFER_UNSUPPORTED:
std::cerr << (R"(GL_FRAMEBUFFER_UNSUPPORTED  
	GL_FRAMEBUFFER_UNSUPPORTED is returned if the combination of internal formats of the attached images violates an implementation-dependent set of restrictions.)") << std::endl;
break;
	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE  
	GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE is returned if the value of GL_RENDERBUFFER_SAMPLES is not the same for all attached renderbuffers; if the value of GL_TEXTURE_SAMPLES is the not same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_RENDERBUFFER_SAMPLES does not match the value of GL_TEXTURE_SAMPLES.
	GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE is also returned if the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures.)") << std::endl;
break;
	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
std::cerr << (R"(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS  
	GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS is returned if any framebuffer attachment is layered, and any populated attachment is not layered, or if all populated color attachments are not from textures of the same target.)") << std::endl;
break;
	case GL_FRAMEBUFFER_COMPLETE:
		std::cout << "Framebuffer complete" << std::endl;
		return;
	default:
		std::cerr << "Unknown Error!" << std::endl;
	}
}

void printVersionInformation( bool printExtensionList)
{
	std::cout	<< "VENDOR: "
				<< glGetString(GL_VENDOR) << std::endl;
	std::cout	<< "RENDERER: "
				<< glGetString(GL_RENDERER) << std::endl;
	std::cout	<< "OpenGL VERSION: "
				<< glGetString(GL_VERSION) << std::endl;
	std::cout	<< "GLSL VERSION: "
				<< glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
	
	GLint n;
	glGetIntegerv(GL_NUM_EXTENSIONS, &n);
	std::cout << n << " AVAILABLE EXTENSIONS";
	
	if(printExtensionList){
		std::cout << ":" << std::endl;
		/* Print list of extensions */
		for (GLint i = 0; i < n; i++) {
			printf("%s\n", glGetStringi(GL_EXTENSIONS, i));
		}
	}
	std::cout << std::endl;
}
