#include "MeshInstance.h"
#include <glm\gtx\string_cast.hpp>
#include "Light.h"

MeshInstance::MeshInstance(){}
MeshInstance::~MeshInstance(){}

void MeshInstance::setURI(const std::string& uri)
{
	mesh = MeshHandler::GetSingleton()[uri];
}

void MeshInstance::setProgram(const std::string& uri)
{
	program = ShaderHandler::GetSingleton()[uri];
}

void MeshInstance::addTexture(const std::string& uri, const std::string& location)
{
	textures.push_back(std::pair<textureptr, std::string>(TextureHandler::GetSingleton()[uri], location));
}
void MeshInstance::setLightList(const std::vector<Light>* lst) {
	pLightList = lst;
}

void MeshInstance::Draw(DestinationFramebufferType framebuffer) const {
	if (!(framebuffer == DestinationFramebufferType::SOLIDOBJECTS && !program->hasTransparency()
		|| framebuffer == DestinationFramebufferType::TRANSPARENTOBJECTS && program->hasTransparency()))
		return;
	float time = SDL_GetTicks() * 0.1f; //Hacky
	glm::mat4 model;
	model = glm::translate(model, position);
	model = glm::translate(model, velocity * time);
	model = glm::rotate(model, rotation.w, glm::vec3(rotation));
	model = glm::rotate(model, rvelocity.w * time, glm::vec3(rvelocity));
	program->Bind();
	int i = 0;
	for (auto texture = textures.begin(); texture != textures.end(); ++texture) {
		Program::UniformLocation textureLocation(texture->second, *program);
		if (textureLocation.isValid())
			textureLocation.BindTexture(i++, *texture->first);
	}
	program->Uniform("ModelMatrix") = model;
	program->Uniform("Lights") = *pLightList;
	program->Uniform("LightsCount") = pLightList->size() < MAX_LIGHTS ? pLightList->size() : MAX_LIGHTS;
	mesh->Draw(framebuffer);
}

void MeshInstance::setMatrices(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj) {
	program->Uniform("ModelMatrix") = model;
	program->Uniform("ViewMatrix") = view;
	program->Uniform("ProjectionMatrix") = proj;
}

void MeshInstance::setTranslation(glm::vec3 translation, glm::vec3 axis, float amount)
{
	position = translation;
	rotation = glm::vec4(axis, amount);
}

void MeshInstance::setVelocity(glm::vec3 translation, glm::vec3 axis, float amount)
{
	velocity = translation;
	rvelocity = glm::vec4(axis, amount);
}