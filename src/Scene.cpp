#include "Scene.h"
#include "json\json.h"
#include <fstream>
#include "Helpers.h"

GLM_CONSTEXPR glm::vec3 UP_VECTOR(0.0, 0.0, 1.0);

Scene::Scene()
{
	valid = false;
}

Scene::Scene(const std::string& filepath)
{
	valid = false;
	Load(filepath);
}

Scene::~Scene()
{
	valid = false;
}

bool Scene::isValid() const { return valid; }

void Scene::Draw(DestinationFramebufferType framebuffer) const {
	for (auto it = objectList.begin(); it != objectList.end(); ++it) {
		it->Draw(framebuffer);
	}
}

void Scene::setMatrices(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj)
{
	projection = proj;
	glm::mat4 _view = glm::lookAt(
		cameraPosition,
		lookAtPosition,
		UP_VECTOR
	);
	for (auto it = objectList.begin(); it != objectList.end(); ++it) {
		it->setMatrices(model, _view, projection);
	}
	}

glm::vec3 Scene::getCamera(){ return cameraPosition; }
glm::vec3 Scene::getLookAt(){ return lookAtPosition; }
void Scene::setCameraLocation(glm::vec3 cam, glm::vec3 lookAt) {
	cameraPosition = cam;
	lookAtPosition = lookAt;
	setMatrices(glm::mat4(), glm::mat4(), projection);
}

void Scene::Load(const std::string &filename)
{
	Json::Reader r;
	Json::Value rootNode;
	std::string error("");
	valid = false;
	std::ifstream file;

	file.open(filename);
	// If all is ok then continue.
	if (file.good() && r.parse(file, rootNode)) {
		setCameraLocation(	toVec3(rootNode["Camera"]["Position"]),
							toVec3(rootNode["Camera"]["LookAt"]));
		auto lights = rootNode["Lights"];
		for (auto it = lights.begin(); it != lights.end(); ++it) {
			auto light = *it;
			glm::vec3 lightPosition = toVec3(light["Position"]);
			float intensity = (float) light["Intensity"].asDouble();
			Light l(lightPosition, intensity);
			lightList.push_back(l);
		}
		auto objects = rootNode["Objects"];
		for (auto it = objects.begin(); it != objects.end(); ++it) {
			auto object = (*it);
			// Create Object and set basic properties.
			Object obj;
			std::cout << object["Name"] << std::endl;
			// Load Mesh Instances for this object.
			auto meshInstances = object["MeshInstance"];
			for (auto it2 = meshInstances.begin(); it2 != meshInstances.end(); ++it2) {
				auto meshInstance = (*it2);
				// Create a Mesh Instance and assign it a program
				MeshInstance meshInst;
				meshInst.setURI(meshInstance["URI"].asString());
				meshInst.setProgram(
					meshInstance["Program"]["URI"].asString()
				);
				// Load & Assign textures to uniform locations
				auto textures = meshInstance["Texture"];
				for (auto it3 = textures.begin(); it3 != textures.end(); ++it3)
					meshInst.addTexture((*it3)["URI"].asString(), (*it3)["Location"].asString());

				meshInst.setLightList((const std::vector<Light>*) &lightList);
				obj.addMeshInstance(meshInst);
			}
			obj.setTranslation(
				toVec3(object["Transform"]["Translation"]),
				toVec3(object["Transform"]["Rotation"]["Axis"]),
				(float) object["Transform"]["Rotation"]["Amount"].asDouble()
				);
			obj.setVelocity(
				toVec3(object["Velocity"]["Translation"]),
				toVec3(object["Velocity"]["Rotation"]["Axis"]),
				(float) object["Velocity"]["Rotation"]["Amount"].asDouble()
				);
			objectList.push_back(obj);
		}
		// TODO: Error checking.
		valid = true;
	}
	// Print out any error messages and finish.
	error = r.getFormatedErrorMessages();
	std::cerr << error;
	file.close();
}
