#include "Helpers.h"

inline std::pair<bool, glm::vec3> toVec3(Json::Value v, const std::string& xstr, const std::string& ystr, const std::string& zstr) {
	std::pair<bool, glm::vec3> returnval;
	if (!v[xstr] || !v[ystr] || !v[zstr]){
		returnval.first = false;
		returnval.second = glm::vec3(-1, -1, -1);
	}
	else{
		returnval.first = true;
		returnval.second = glm::vec3(v[xstr].asDouble(), v[ystr].asDouble(), v[zstr].asDouble());
	}
	return returnval;
}

glm::vec3 toVec3(Json::Value v) {
	if (v.isArray() && v.size() == 3)
		return glm::vec3(v[0U].asDouble(), v[1U].asDouble(), v[2U].asDouble());
	if (v.isObject()) {
		std::pair<bool, glm::vec3> result;
		if ((result = toVec3(v, "x", "y", "z")).first) { return result.second; }
		if ((result = toVec3(v, "r", "g", "b")).first) { return result.second; }
		if ((result = toVec3(v, "u", "v", "w")).first) { return result.second; }
		if ((result = toVec3(v, "pitch", "yaw", "roll")).first) { return result.second; }
	}
	std::cerr << "Warning: invalid vec3 at :\n" << v.toStyledString() << std::endl;
	return glm::vec3(-1, -1, -1);
}