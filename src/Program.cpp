#include "Program.h"
#include "Shader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

#include "GLError.h"

#ifndef GL_LINEAR_MIPMAP_NEAREST
#define GL_LINEAR_MIPMAP_NEAREST GL_LINEAR
#endif

Program::Program(const std::string& filepath) : programName(filepath){
	handle = program_handle_invalid;
	transparency = false;
	Load(filepath);
}

Program::~Program() {
	glDeleteProgram(handle);
}

program_handle Program::getHandle() const
{
	return handle;
}

void Program::Bind() const
{
	glUseProgram(getHandle());
}

bool Program::isValid() const
{
	return handle != program_handle_invalid;
}

const std::string Program::getName() {
	return programName;
}

static GLenum getType(const std::string& type){
	if(type.compare("VERTEX") == 0)
		return GL_VERTEX_SHADER;
	if(type.compare("FRAGMENT") == 0)
		return GL_FRAGMENT_SHADER;
	if(type.compare("GEOMETRY") == 0)
		return GL_GEOMETRY_SHADER;
	if(type.compare("TESS") == 0)
		return GL_TESS_CONTROL_SHADER;
	return GL_INVALID_ENUM;
}

void Program::Load(const std::string& filename) {
	// Open the file
	std::vector<std::pair<std::string, GLenum>> attachedShaders;
	std::ifstream programFile(filename);
	if(!programFile.good()) {
		std::cerr << "Could Not Open File '" << filename << "'\n";
		return;
	}
	do {
		std::string line;
		std::getline(programFile, line);
		std::istringstream iline(line);
		// If line is empty, Skip it.
		if(!iline.good() || line.compare("") == 0)
			continue;

		if (line.compare("#transparent") == 0) {
			setTransparency(true);
		} else {
		std::string path, type;
		std::getline(iline, path, ':');
			if (iline.good()) {
			std::getline(iline, type, ';');
			attachedShaders.push_back(std::pair<std::string, GLenum>(path, getType(type)));
			}
			else {
			// No second half to the shader.
				std::cerr << "Expected ':'";
			}
		}
	} while(programFile.good());
	// Create Program
	handle = glCreateProgram();
	// Attach Shaders:
	for(auto s = attachedShaders.begin();
		s != attachedShaders.end();
		s ++)
	{
		Shader shader(s->first, s->second);
		glAttachShader(getHandle(), shader.getHandle());
	}
	// Link
	glLinkProgram(getHandle());
	checkGLShaderStatus(getHandle(), GL_LINK_STATUS, false);
	checkGL("Linking shader");
	// BUG: Without bind here Getting uniform Location on linux doesnt work? - Mesa 10.2.0
	Bind(); 
	// Shaders will go out of scope and be deleted automatically.
	return;
}

Program::operator GLuint() {
	return getHandle();
}

Program::UniformLocation Program::Uniform(const std::string& locationName) {
	return UniformLocation(locationName, *this);
}

Program::UniformLocation Program::operator[](const std::string& locationName)
{
	return Uniform(locationName);
}

Program::UniformLocation::UniformLocation(const std::string& loc, Program& p) : locationName(loc), program(&p)
{
	program->Bind();
	// Get the location in GPU memory to use for setting the uniform
	location = glGetUniformLocation(*program, locationName.c_str());
	checkGL("Getting Uniform Location", false);
}

Program::UniformLocation::operator GLuint() {
	return getHandle();
}

Program::UniformLocation::operator bool()
{
	return isValid();
}

bool Program::UniformLocation::isValid()
{
	return location != -1;
}

Program::UniformLocation& Program::UniformLocation::operator= (const float& lhs) 
{
	if (isValid()) {
		program->Bind();
		glUniform1f(location, lhs);
		d_checkGL("Setting uniform");
	}
	return *this;
}

Program::UniformLocation& Program::UniformLocation::operator= (const glm::mat4& lhs)
{
	if (isValid()) {
		program->Bind();
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(lhs));
		d_checkGL("Setting uniform");
	}
	return *this;
}

Program::UniformLocation& Program::UniformLocation::operator= (const GLuint& lhs)
{
	if (isValid()){
		program->Bind();
		glUniform1i(location, lhs);
		d_checkGL("Setting uniform");
	}
	return *this;
}

Program::UniformLocation& Program::UniformLocation::operator= (const glm::vec4& lhs)
{
	if (isValid()){
		program->Bind();
		glUniform4fv(location, 1, glm::value_ptr(lhs));
		d_checkGL("Setting uniform");
	}
	return *this;
}

Program::UniformLocation& Program::UniformLocation::operator= (const std::vector<Light>& lhs)
{
	if (isValid()){
		program->Bind();
		glUniform4fv(location, lhs.size() < MAX_LIGHTS ? lhs.size() : MAX_LIGHTS, (GLfloat*)lhs.data());
		d_checkGL("Setting uniform");
	}
	return *this;
}

void Program::UniformLocation::BindTexture(int textureUnit, const Texture& texture) {
	if (isValid()){
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		texture.Bind();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		program->Bind();
		glUniform1i(location, textureUnit);
		d_checkGL("Setting uniform");
	}
	else {
		std::cerr << "Cant Bind Texture : " << locationName << '@' << program->getName() << std::endl;
	}
}

void Program::UniformLocation::BindTexture(int textureUnit, const Sampler& sampler, const Texture& texture) {
	if (isValid()){
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		texture.Bind();
		glBindSampler(textureUnit, sampler.getHandle());
		glBindTexture(GL_TEXTURE_2D, texture.getHandle());
		program->Bind();
		glUniform1i(location, textureUnit);
	}
	else {
		std::cerr << "Cant Bind Texture : " << locationName << std::endl;
	}
}

Program::UniformLocation::uniform_handle Program::UniformLocation::getHandle() {
	return location;
}

bool Program::hasTransparency() {
	return transparency;
}

void Program::setTransparency(bool t) {
	transparency = t;
}