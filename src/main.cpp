#ifdef __WIN32__
#	include "stdafx.h"
#	include <Windows.h>
#endif

#include <iostream>
#include <vector>
#include "GLInclude.h"
#include "GLError.h"

#include "SceneHandler.h"
#include "FrameBuffer.h"
#include "ShaderHandler.h"
#include "TaskHandler.h"

// For Mesa 10.1(unstable) You must Request an OpenGL 3.3 (core) context.
//  to get OpenGL 3.3 (Core) Functionality
//	Warning: This Disables Deprecated Functionality!
enum OpenGLContextInformation { 
	glMinimumMajorVersionNumber = 3, 
	glMinimumMinorVersionNumber = 3,
	glContextProfileMask = SDL_GL_CONTEXT_PROFILE_CORE
};

bool quit = false;
SDL_Renderer *ren = nullptr;
SDL_Window *win = nullptr;

void render(std::vector<any_ptr> arguments) {
	// Create an OpenGL rendering context and exit if this failed
	SDL_GLContext context = SDL_GL_CreateContext(win);
	if (context == nullptr) exit(EXIT_FAILURE);
	d_checkGL("Creating context");
	// Initalize the GLEW library for OpenGL extenions
	// Note: we must do this AFTER creating the context or horrific faults will occur
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK){
		std::cerr << "GLEW : " << glewGetErrorString(glewError) << std::endl;
		exit(EXIT_FAILURE);
	}
	// Glew produces errors on some platforms that should be ignored
	clearGlErrors(); 

	printVersionInformation(true);

	// Load Shader program
	sceneptr scene = SceneHandler::GetSingleton()["scene/bathroom.json"];
	d_checkGL("Creating Scene");
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glm::vec2 screen(1024, 1024); //screen size
	glm::mat4 proj = glm::perspective(45.0f, (float)screen.x / (float)screen.y, 1.0f, 400.0f);
	scene->setMatrices(glm::mat4(), glm::mat4(), proj);

	glm::mat4 model; // Store the model matrix

	Framebuffer fbSolidObjects;
	Texture solidObjectsDepthBuffer(nullptr, 1024, 1024, GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_FLOAT);
	fbSolidObjects.attachToTexture(solidObjectsDepthBuffer, GL_DEPTH_ATTACHMENT);
	Texture solidObjectsTexture(nullptr, 1024, 1024, GL_RGBA, GL_RGBA);
	fbSolidObjects.attachToTexture(solidObjectsTexture, 0);
	checkFramebufferStatus(GL_FRAMEBUFFER);
	d_checkGL("Creating Solid objects Framebuffer");

	Framebuffer fbNonSolidObjects;
	Texture nonSolidObjectsDepthBuffer(nullptr, 1024, 1024, GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, GL_FLOAT);
	fbNonSolidObjects.attachToTexture(nonSolidObjectsDepthBuffer, GL_DEPTH_ATTACHMENT);
	Texture nonSolidObjectsTexture(nullptr, 1024, 1024, GL_RGBA, GL_RGBA);
	fbNonSolidObjects.attachToTexture(nonSolidObjectsTexture, 0);
	checkFramebufferStatus(GL_FRAMEBUFFER);
	d_checkGL("Creating Non-Solid Objects Framebuffer");

	meshptr screenMesh = MeshHandler::GetSingleton()["model/screen.3ds"];
	programptr screenShader = ShaderHandler::GetSingleton()["shader/screen.prog"];

	const int nsColor = 0, sColor = 1, nsDepth = 2, sDepth = 3;

	screenShader->Uniform("nonSolidObject").BindTexture(nsColor, nonSolidObjectsTexture);
	screenShader->Uniform("solidObject").BindTexture(sColor, solidObjectsTexture);
	screenShader->Uniform("nonSolidDepth").BindTexture(nsDepth, nonSolidObjectsDepthBuffer);
	screenShader->Uniform("solidDepth").BindTexture(sDepth, solidObjectsDepthBuffer);

	float dtime;
	Uint32 old_time, current_time;
	current_time = SDL_GetTicks();

	// Render Loop
	SDL_Event event;
	bool quit = false;
	while (!quit) {
		old_time = current_time;
		current_time = SDL_GetTicks();
		dtime = (current_time - old_time) / 1000.0f;

		glm::vec3 cameraLocation = scene->getCamera();
		glm::vec3 lookAtLocation = scene->getLookAt();

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT ||
				event.type == SDL_KEYDOWN
				&& event.key.keysym.sym == SDLK_ESCAPE) {
				quit = true;
			}
			if (event.type == SDL_KEYDOWN) {
				switch (event.key.keysym.sym) {
				case SDLK_w:
					cameraLocation.y += dtime * 10;
					break;
				case SDLK_s:
					cameraLocation.y -= dtime * 10;
					break;
				case SDLK_a:
					cameraLocation.x += dtime * 10;
					break;
				case SDLK_d:
					cameraLocation.x -= dtime * 10;
					break;
				default:
					cameraLocation = scene->getCamera();
					lookAtLocation = scene->getLookAt();
					break;
				}
				scene->setCameraLocation(cameraLocation, lookAtLocation);
			}
		}
		glClearDepthf(700.0f);
		glClearColor(0.f, 0.f, 0.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		fbSolidObjects.Bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		scene->Draw(DestinationFramebufferType::SOLIDOBJECTS);
		fbSolidObjects.BindDefault();

		fbNonSolidObjects.Bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		scene->Draw(DestinationFramebufferType::TRANSPARENTOBJECTS);
		fbNonSolidObjects.BindDefault();

		nonSolidObjectsTexture.Bind(nsColor);
		solidObjectsTexture.Bind(sColor);
		nonSolidObjectsDepthBuffer.Bind(nsDepth);
		solidObjectsDepthBuffer.Bind(sDepth);
		screenShader->Bind();
		screenMesh->Draw(DestinationFramebufferType::OUTPUT);

		glFlush();
		SDL_GL_SwapWindow(win);
		d_checkGL("Animating");
	}

	// Clean up
	SDL_GL_DeleteContext(context);
}

void update(std::vector<any_ptr> arguments) {
	// Update loop does nothing
	do {
		SDL_Delay(200);
	} while (!quit);
}

int main(int argc, char* argv[])
{
	// Set Context Request Parameters
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, glContextProfileMask);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, glMinimumMajorVersionNumber);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, glMinimumMinorVersionNumber);

	// Initalize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) exit(EXIT_FAILURE);
	
	// Initialize SDL_Image
	if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) == -1) exit(EXIT_FAILURE);
	
	// Create the SDL window and exit if this failed
	win = SDL_CreateWindow("My even more awesome SDL/OGL program", 100, 100, 1024, 1024, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (win == nullptr) exit(EXIT_FAILURE);

	// Create the SDL rendering context and exit if this failed
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) exit(EXIT_FAILURE);
	
	// Program code here
	Delegate renderer;
	renderer.Bind<&render>();

	Delegate updater;
	updater.Bind<&update>();

	TaskHandler taskManager;
	taskManager.CreateSpecialisedThread("Render Thread");
	taskManager.GetSpecialisedThread("Render Thread").QueueTask(renderer);
	taskManager.CreateSpecialisedThread("Update Thread");
	taskManager.GetSpecialisedThread("Update Thread").QueueTask(updater);
	
	char c;
	std::cin >> c;

	SDL_DestroyRenderer(ren); 
	SDL_DestroyWindow(win); 
	IMG_Quit();
	SDL_Quit();
	return 0;
}