#include "Texture.h"
#include <iostream>
#include <cstring> //memcpy
#include "GLError.h"

Texture::Texture(const std::string& filepath) {
	// Initialise as null texture (not loaded))
	handle = texture_handle_invalid;
	Load(filepath);
}

Texture::Texture(byte* pixelData, int w, int h, GLenum internalFormat, GLenum format, GLenum type) {
	glGenTextures(1, &handle);
	Bind();
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0, format, type, pixelData);
	textureFormat = format;
	width = w;
	height = h;
}

Texture::Texture(texture_handle t, int w, int h, GLenum f) {
	textureFormat = f;
	width = w;
	height = h;
	handle = t;
}

Texture::~Texture() {
	glDeleteTextures(1, &handle);
}

texture_handle Texture::getHandle() const
{
	return handle;
}

void Texture::Bind() const
{
	glBindTexture(GL_TEXTURE_2D, handle);
}
void Texture::Bind(int textureUnit) const
{
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	Bind();
}
void Texture::BindDefault() const
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

bool Texture::isValid() const
{
	return glIsTexture(handle) == GL_TRUE;
}

void Texture::LoadDefault() {
	std::cerr << "Using Default Texture" << std::endl;
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);
	// Black/white checkerboard
	float pixels[] = {
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f
	};
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, pixels);
}

/* Based on http://www.gribblegames.com/articles/game_programming/sdlgl/invert_sdl_surfaces.html */
inline static void InvertImage(int pitch, int height, byte* image_pixels) {
	byte* currentRow = new byte[pitch];
	if (currentRow == nullptr) {
		std::cerr << "SDL_Image: Invert: Insufficient Memory\n";
		return;
	}
	// If height is odd, don't need to swap middle row
	int height_div_2 = (int)(height / 2);
	for (int i = 0; i < height_div_2; ++i) 	{
		std::memcpy(
			currentRow,
			&image_pixels[pitch * i],
			pitch
		);
		std::memcpy(
			&image_pixels[pitch * i], 
			&image_pixels[pitch * (height - i - 1)],
			pitch
		);
		std::memcpy(
			&image_pixels[pitch * (height - i - 1)],
			currentRow,
			pitch
		);
	}
	delete[] currentRow;
}

void Texture::Load(const std::string& filename) {
	/* SDL_Image to OpenGL Image: (http://www.geometrian.com/programming/tutorials/texturegl/index.php) */
	SDL_Surface* surface = IMG_Load(filename.c_str());
	if(!surface){
		std::cerr << "SDL_Image : " << IMG_GetError() << std::endl;
		LoadDefault();
		return;
	}
	
	// Check that the image's width is a power of 2 
	if ((surface->w & (surface->w - 1)) != 0) {
		std::cerr << "SDL_Image: Image Size (Width) is not a power of 2";
	}
	// Also check if the height is a power of 2 
	if ((surface->h & (surface->h - 1)) != 0) {
		std::cerr << "SDL_Image: Image Size (Height) is not a power of 2";
	}

	/* OpenGL Reads from Bottom-left */
	InvertImage(surface->pitch, surface->h, (byte*)surface->pixels);

	/* Upload texture to graphics card. */
	glGenTextures(1, &handle);
	glBindTexture(GL_TEXTURE_2D, handle);

	/* Set the color mode @todo: Figure it out based on image format...*/
	GLenum textureFormat = GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, surface->w, surface->h, 0, textureFormat, GL_UNSIGNED_BYTE, surface->pixels);

	// Free the surface
	SDL_FreeSurface(surface);
	d_checkGL("Loading Image");
	std::cout << "Loaded Image: '" << filename << "' (" << surface->w << "x" << surface->h << ")\n";
}