#include "ShaderHandler.h"

programptr ShaderHandler::operator[](const std::string &filename) {
	program_iterator result;
	result = mHandleMap.find(filename);
	if (result != mHandleMap.end()) {
		return result->second;
	}
	else {
		programptr loadResult(new Program(filename));
		if (loadResult->isValid()) {
			mHandleMap.insert(program_pair(filename, loadResult));
		}
		else {
			std::cerr << "Failed to load shader program (\"" << filename << "\")\n";
		}
		return loadResult;
	}
}

size_t ShaderHandler::size() { return mHandleMap.size(); }
