#include "MeshHandler.h"

meshptr MeshHandler::operator[](const std::string &filename) {
	mesh_iterator result;
	result = mHandleMap.find(filename);
	if(result != mHandleMap.end()) {
		return result->second;
	} else {
		meshptr loadResult(new Mesh(filename));
		if(loadResult->isValid()) {
			mHandleMap.insert(mesh_pair(filename, loadResult));
		} else {
			std::cerr << "Failed to load mesh (\"" << filename << "\")\n";
		}
		return loadResult;
	}
}

size_t MeshHandler::size() { return mHandleMap.size(); }
