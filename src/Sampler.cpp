#include "Sampler.h"

Sampler::Sampler(GLenum magFilter, GLenum minFilter, GLenum wrap) {
	Init(magFilter, minFilter, wrap);
}
Sampler::~Sampler(){}

void Sampler::Init(GLenum magFilter, GLenum minFilter, GLenum wrap) {
	glGenSamplers(1, &handle);
	glSamplerParameteri(handle, GL_TEXTURE_MAG_FILTER, magFilter);
	glSamplerParameteri(handle, GL_TEXTURE_MIN_FILTER, minFilter);
	glSamplerParameteri(handle, GL_TEXTURE_WRAP_S, wrap);
	glSamplerParameteri(handle, GL_TEXTURE_WRAP_T, wrap);
}

sampler_handle Sampler::getHandle() const {
	return handle;
}
