﻿#include "FrameBuffer.h"
#include <iostream>

Framebuffer::Framebuffer(int sizex, int sizey)
{
	width = sizex;
	height = sizey;
	glGenFramebuffers(1, &handle);
	if (glewIsExtensionSupported("ARB_framebuffer_no_attachments")) {
		Bind();
		glFramebufferParameteri(GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_WIDTH, sizex);
		glFramebufferParameteri(GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_HEIGHT, sizey);
		glFramebufferParameteri(GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_SAMPLES, 0);
		BindDefault();
	}
}

Framebuffer::~Framebuffer()
{
	glDeleteFramebuffers(1, &handle);
}

bool Framebuffer::isValid() const
{
	Bind();
	return glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
}

handle Framebuffer::getHandle() const
{
	return handle;
}

void Framebuffer::Bind() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, handle);
}

void Framebuffer::BindDefault() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::attachToTexture(const Texture& texture, int attachmentPoint)
{
	Bind();
	glFramebufferTexture2D(GL_FRAMEBUFFER, getAttachment(attachmentPoint), GL_TEXTURE_2D, texture.getHandle(), 0);
}