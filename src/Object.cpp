#include "Object.h"
#include "Helpers.h"

Object::Object(){}
Object::~Object(){}

static bool validateRotation(glm::vec3& rotation, float amount) {
	if (rotation.x == 0 && rotation.y == 0 && rotation.z == 0){
		std::cerr << "Warning: Rotation Axis must not be (0,0,0)\n";
		if (amount == 0){
			std::cerr << "\tFixing : New rotation axis (1, 0, 0)\n";
			rotation.x = 1;
		}
		else {
			std::cerr << "Error: Invalid rotation around null axis\n";
			return false;
		}
	}
	return true;
}

void Object::setTranslation(glm::vec3 translation, glm::vec3 rotation, float amount)
{
	validateRotation(rotation, amount);
	for (auto it = meshInstances.begin(); it != meshInstances.end(); ++it) {
		it->setTranslation(translation, rotation, radians(amount));
	}
}

void Object::setVelocity(glm::vec3 translation, glm::vec3 rotation, float amount)
{
	validateRotation(rotation, amount);
	for (auto it = meshInstances.begin(); it != meshInstances.end(); ++it) {
		it->setVelocity(translation, rotation, radians(amount));
	}
}

void Object::addMeshInstance(const MeshInstance& meshInstance)
{
	meshInstances.push_back(meshInstance);
}

void Object::Draw(DestinationFramebufferType framebuffer) const {
	for (auto it = meshInstances.begin(); it != meshInstances.end(); ++it) {
		it->Draw(framebuffer);
	}
}

void Object::setMatrices(const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj) {
	for (auto it = meshInstances.begin(); it != meshInstances.end(); ++it) {
		it->setMatrices(model, view, proj);
	}
}