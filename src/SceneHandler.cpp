#include "SceneHandler.h"

sceneptr SceneHandler::operator[](const std::string &filename) {
	scene_iterator result;
	result = mHandleMap.find(filename);
	if (result != mHandleMap.end()) {
		return result->second;
	}
	else {
		sceneptr loadResult(new Scene(filename));
		if (loadResult->isValid()) {
			mHandleMap.insert(scene_pair(filename, loadResult));
		}
		else {
			std::cerr << "Failed to load scene (\"" << filename << "\")\n";
		}
		return loadResult;
	}
}

size_t SceneHandler::size() { return mHandleMap.size(); }