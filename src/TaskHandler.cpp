#include "TaskHandler.h"
#include <iostream> // TODO: Logging so that this can be recorded and seen in-game
#include <vector>
#include <SDL_timer.h> //SDL_Delay();

TaskHandler::TaskHandler()
{
}
TaskHandler::~TaskHandler()
{
	// Each task
		// Await task
}

bool TaskHandler::CreateWorkerThread()
{
	workerThreads.emplace_back("General Worker");
	return true;
}

bool TaskHandler::CreateSpecialisedThread(const std::string& threadId)
{
	specialisedThreads.emplace(threadId, threadId);
	return true;
}

TaskHandler::TaskCollection& TaskHandler::GetWorkerThread()
{
	if (workerThreads.size() == 0 && !CreateWorkerThread())
		std::cerr << "Could not create initial thread!"  << std::endl;
	auto t = workerThreads.begin();
	for (auto it = workerThreads.begin(); it != workerThreads.end(); ++it) {
		if (it->Size() < t->Size())
			t = it;
	}
	assert(t != workerThreads.end() && "There are no more jobs");
	return *t;
}

TaskHandler::TaskCollection& TaskHandler::GetSpecialisedThread(std::string threadName)
{
	assert(specialisedThreads.find(threadName) != specialisedThreads.end() && "Cant find a thread by that name");
	return specialisedThreads.at(threadName);
}

int __cdecl Scheduler(void* taskCollection) 
{
	const std::vector<any_ptr> arguments;
	TaskHandler::TaskCollection* thisptr = (TaskHandler::TaskCollection*)taskCollection;
	do {
		while (thisptr->Size() > 0) {
			// Get highest priority job
			Task task = thisptr->jobs.front();
			// Run it
			task.second.Invoke(arguments);
			// Remove it from the list
			thisptr->jobs.pop();
		}
		SDL_Delay(50);
	} while (true);
}

TaskHandler::TaskCollection::TaskCollection() {}
static size_t threadCounter;
TaskHandler::TaskCollection::TaskCollection(const std::string& name)
{
	size_t threadNameSize = sizeof("Scheduler_0x000:") + name.size();
	char* threadName = new char[threadNameSize];
	sprintf_s(threadName, threadNameSize, "Scheduler_0x%03x:%s", threadCounter++, name.c_str());
	thread = SDL_CreateThread(Scheduler, threadName, this);
	if (thread == nullptr)
		std::cerr << "SDL_CreateThread failed:" << SDL_GetError() << std::endl;
	delete threadName;
}

TaskHandler::TaskCollection::~TaskCollection()
{
}

size_t TaskHandler::TaskCollection::Size()
{ 
	return jobs.size();
}

int TaskHandler::TaskCollection::Await() 
{
	int status;
	SDL_WaitThread(thread, &status);
	return status;
}