#include "Shader.h"
#include <iostream>
#include <fstream>
#include "GLError.h"

Shader::Shader(const std::string& filepath, GLenum t) {
	// Initialise as null texture (not loaded))
	handle = shader_handle_invalid;
	type = t;
	Load(filepath);
}

Shader::Shader(shader_handle h, GLenum t) {
	handle = h;
	type = t;
}

Shader::~Shader() {
	glDeleteShader(handle);
}

shader_handle Shader::getHandle() const
{
	return handle;
}

void Shader::Bind() const
{
	return;
}

bool Shader::isValid() const
{
	return handle != shader_handle_invalid;
}

void Shader::Load(const std::string& filename) {
	// Open the file
	std::ifstream shaderFile(filename);
	if (shaderFile.good()) {
		// Get the size
		shaderFile.seekg(0, shaderFile.end);
		size_t fileSize = (size_t) shaderFile.tellg();
		shaderFile.seekg(0, shaderFile.beg);
		// Allocate space & Copy
		GLchar* shaderSource = new GLchar[fileSize + 1];
		shaderFile.read(shaderSource, fileSize);
		shaderSource[fileSize] = '\0';
		// Compile shader
		handle = Shader::Build((const GLchar**)&shaderSource, type);
		// Free and return.
		delete[] shaderSource;
	}
	else {
		std::cerr << "Could not Load Shader '" << filename << "' : Could not open file\n";
		handle = shader_handle_invalid;
	}
}

// General function for compiling a shader.
shader_handle Shader::Build(const GLchar** source, GLenum type) {
	// Get space on the GPU for the shader
	GLuint shaderId = glCreateShader(type);
	// Upload source code
	glShaderSource(shaderId, 1, source, NULL);
	// Compile the shader
	glCompileShader(shaderId);
	// See how the compilation went
	checkGLShaderStatus(shaderId, GL_COMPILE_STATUS, true);
	checkGL(ShaderTypeName(type));
	return shaderId;
}
