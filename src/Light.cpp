#include "Light.h"

Light::Light(glm::vec3 p, float i) : position(p), intensity(i){}
Light::operator glm::vec4() {
	return glm::vec4(position, intensity);
}

glm::vec3 Light::getPosition() {
	return position;
}

float Light::getIntensity() {
	return intensity;
}